<?php
class Pages extends CI_Controller {



	public function view($page = 'home')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		$list_base_URL='http://localhost/ci_rest_ljm/api/list';
		$get_list_array= json_decode(file_get_contents($list_base_URL), true);
			$parse_array = array(
			'list_entries' => array(
			)
		);
		$parse_array['list_entries']=array_merge($get_list_array, $parse_array['list_entries']);
		$this->load->helper('url');
		$this->load->library('parser');
		$this->load->view('templates/header');
		$this->parser->parse('pages/'.$page, $parse_array); // Parsed value must be an array!
		//sleep(1);
		$this->parser->parse('templates/footer', $parse_array);// if it is view then it gets loaded before the main content TODO: find better solution


	}
}

